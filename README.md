# CS 373: Software Engineering Collatz Repo

* Name: Ishan Phadke

* EID: iap392

* GitLab ID: ishanphadke

* HackerRank ID: ishanphadke1

* Git SHA: 7904e121f9a462e391d7dad2a571c3f9c116c387

* GitLab Pipelines: https://gitlab.com/ishanphadke/cs373-collatz/-/pipelines

* Estimated completion time: 12 hours

* Actual completion time: 8.5 hours

* Comments: I really liked the project's ability to make CI/CD practices clear.
