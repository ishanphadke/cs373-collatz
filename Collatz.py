#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, Tuple

# ------------
# collatz_read
# ------------

# Add caching
memo = {}


def collatz_read(s: str) -> Tuple[int, int]:
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return (int(a[0]), int(a[1]))


# ------------
# collatz_eval
# ------------


def collatz_eval(t: Tuple[int, int]) -> Tuple[int, int, int]:
    """
    t a tuple of two ints
    return a tuple of three ints
    """
    i, j = t
    beg = min(i, j)
    end = max(i, j)
    max_cycles = -1
    assert beg <= end, "Input mismatch, i > j"
    assert beg < 1000000, "Beginning of range invalid"
    assert end < 1000000, "End of range invalid"

    # optimize the range if beg is less than end/2 +1
    beg = max(beg, (end >> 1) + 1)

    # start looping through optimized range
    for num in range(beg, end + 1):
        cycles = 1
        start = num
        # check if num's cycle length has already been computed
        if start not in memo:
            while num > 1:
                if num % 2 == 0:
                    # divide by 2 if even, faster using >> instead of /
                    num >>= 1
                else:
                    # optimized the resulting even number if odd, reducing steps taken by 1
                    num += (num >> 1) + 1
                    cycles += 1
                cycles += 1
                # if the current num's cycle length has been stored, just add to current count
                if num in memo:
                    cycles += memo[num] - 1
                    break
            # store final cycle length of the initial num
            memo[start] = cycles
        else:
            # cycle length has already been computed
            cycles = memo[start]
        # store the max cycles so far
        max_cycles = max(max_cycles, cycles)

    # make sure the max cycles were actually computed
    assert max_cycles > 0, "Max cycles calculated incorrectly"
    return (i, j, max_cycles)


# -------------
# collatz_print
# -------------


def collatz_print(sout: IO[str], t: Tuple[int, int, int]) -> None:
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(sin: IO[str], sout: IO[str]) -> None:
    """
    sin  a reader
    sout a writer
    """
    for s in sin:
        collatz_print(sout, collatz_eval(collatz_read(s)))
